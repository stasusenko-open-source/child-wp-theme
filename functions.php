<?php
function child_wp_theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/css/main.css', array(), null );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/css/main.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'child_wp_theme_enqueue_styles' );

function child_wp_theme_setup() {
    load_child_theme_textdomain( 'child-wp-theme', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'child_wp_theme_setup' );
